/**
 * A 1-bit two's complement signed integer (in the inclusive range [-1, 0])
 */
export type Sint1 = -1 | 0;

/**
 * A 2-bit two's complement signed integer (in the inclusive range [-2, 1])
 */
export type Sint2 = -2 | -1 | 0 | 1;

/**
 * A 3-bit two's complement signed integer (in the inclusive range [-4, 3])
 */
export type Sint3 = -4 | -3 | -2 | -1 | 0 | 1 | 2 | 3;

/**
 * A 4-bit two's complement signed integer (in the inclusive range [-8, 7])
 */
export type Sint4 = -8 | -7 | -6 | -5 | -4 | -3 | -2 | -1 | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7;

/**
 * A 5-bit two's complement signed integer (in the inclusive range [-16, 15])
 */
export type Sint5 = number;

/**
 * A 6-bit two's complement signed integer (in the inclusive range [-32, 31])
 */
export type Sint6 = number;

/**
 * A 7-bit two's complement signed integer (in the inclusive range [-64, 63])
 */
export type Sint7 = number;

/**
 * A 8-bit two's complement signed integer (in the inclusive range [-128, 127])
 */
export type Sint8 = number;

/**
 * A 16-bit two's complement signed integer (in the inclusive range [-32768, 32767])
 */
export type Sint16 = number;

/**
 * A 24-bit two's complement signed integer (in the inclusive range [-8388608, 8388607])
 */
export type Sint24 = number;

/**
 * A 32-bit two's complement signed integer (in the inclusive range [-2147483648, 2147483647])
 */
export type Sint32 = number;
