/**
 * A 16 bit floating point number, corresponds to the IEEE 754 binary16
 * - 5 bits for the exponent
 * - 1 bit for the sign
 * - 10 bits for the mantissa
 */
export type Float16 = number;

/**
 * A 32 bit floating point number, corresponds to the IEEE 754 binary32
 * - 8 bits for the exponent
 * - 1 bit for the sign
 * - 23 bits for the mantissa
 */
export type Float32 = number;

/**
 * A 64 bit floating point number, corresponds to the IEEE 754 binary64
 * - 11 bits for the exponent
 * - 1 bit for the sign
 * - 52 bits for the mantissa
 */
export type Float64 = number;
