import {Float64} from "./floating-point";
import {Uint8} from "./uint";

/**
 * Represents a point in the sRGB color-space.
 * This is the recommended color-space for storage and display.
 *
 * @param D Color depth
 */
export interface SRgb<D extends number = Uint8> {
  r: D;
  g: D;
  b: D;
}

/**
 * Represents a point in the sRGB color-space with alpha (transparancy) information.
 * This is the recommended color-space for storage and display.
 * It is not specified if the RGB values are _straight_ or _premultiplied_ by the alpha value.
 *
 * @param D Color depth
 */
export interface SRgba<D extends number = Uint8> extends SRgb<D> {
  a: D;
}

/**
 * Represents a point in the sRGB color-space with alpha (transparancy) information.
 * This is the recommended color-space for storage and display.
 * The RGB values are not premultiplied by the alpha value (they are straight).
 *
 * @param D Color depth
 */
export interface StraightSRgba<D extends number = Uint8> extends SRgba<D> {
}

/**
 * Represents a point in the sRGB color-space with alpha (transparancy) information.
 * This is the recommended color-space for storage and display.
 * The RGB values are premultiplied by the alpha value (they are straight).
 *
 * @param D Color depth
 */
export interface PremultipliedSRgba<D extends number = Uint8> extends SRgba<D> {
}

/**
 * Represents a point in the linear RGB color-space.
 * This is the recommended color-space for physically-accurate color processing.
 *
 * @param D Color depth
 */
export interface LinearRgb<D extends number = Float64> {
  r: D;
  g: D;
  b: D;
}

/**
 * Represents a point in the linear RGB color-space with alpha (transparancy) information.
 * This is the recommended color-space for storage and display.
 * It is not specified if the RGB values are _straight_ or _premultiplied_ by the alpha value.
 *
 * @param D Color depth
 */
export interface LinearRgba<D extends number = Uint8> extends LinearRgb<D> {
  a: D;
}
