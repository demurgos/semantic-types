/**
 * A 1-bit unsigned integer (in the inclusive range [0, 1])
 */
export type Uint1 = 0 | 1;

/**
 * A 2-bit unsigned integer (in the inclusive range [0, 3])
 */
export type Uint2 = 0 | 1 | 2 | 3;

/**
 * A 3-bit unsigned integer (in the inclusive range [0, 7])
 */
export type Uint3 = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7;

/**
 * A 4-bit unsigned integer (in the inclusive range [0, 15])
 */
export type Uint4 = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15;

/**
 * A 5-bit unsigned integer (in the inclusive range [0, 31])
 */
export type Uint5 = number;

/**
 * A 6-bit unsigned integer (in the inclusive range [0, 63])
 */
export type Uint6 = number;

/**
 * A 7-bit unsigned integer (in the inclusive range [0, 127])
 */
export type Uint7 = number;

/**
 * A 8-bit unsigned integer (in the inclusive range [0, 255])
 */
export type Uint8 = number;

/**
 * A 16-bit unsigned integer (in the inclusive range [0, 65535])
 */
export type Uint16 = number;

/**
 * A 24-bit unsigned integer (in the inclusive range [0, 16777215])
 */
export type Uint24 = number;

/**
 * A 32-bit unsigned integer (in the inclusive range [0, 4294967295])
 */
export type Uint32 = number;
