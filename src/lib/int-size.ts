/**
 * Represents a signed integer of the size of the architecture of the current system
 */
export type SintSize = number;

/**
 * Represents an unsigned integer of the size of the architecture of the current system
 */
export type UintSize = number;
