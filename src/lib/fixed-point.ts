/**
 * Fixed-point number with a 8-bit signed integer part and 8-bit unsigned fractional part
 */
export type Sfixed8p8 = number;

/**
 * Fixed-point number with a 8-bit unsigned integer part and 8-bit unsigned fractional part
 */
export type Ufixed8p8 = number;

/**
 * Fixed-point number with a 8-bit signed integer part and 8-bit unsigned fractional part
 */
export type Sfixed16p16 = number;

/**
 * Fixed-point number with a 8-bit unsigned integer part and 8-bit unsigned fractional part
 */
export type Ufixed16p16 = number;
