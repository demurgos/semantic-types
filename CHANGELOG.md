# 0.1.1 (2017-07-11)

- **[Feature]** Add types for sRGB and linear RGB colors.
- **[Documentation]** Fix documentation of unsigned integer. It was stating that they were representing
  signed integers, while they represent unsigned integers.

# 0.1.0 (2017-07-10)

First release.

- **[Feature]** Define numeric types: `sint`, `uint`, `floating-point`, `fixed-point`, `int-size`.
