"use strict";

const buildTools = require("demurgos-web-build-tools");
const gulp = require("gulp");
const typescript = require("typescript");

// Project-wide options
const projectOptions = Object.assign(
  {},
  buildTools.config.DEFAULT_PROJECT_OPTIONS,
  {
    root: __dirname
  }
);

// `lib` target
const libTarget = Object.assign(
  {},
  buildTools.config.LIB_TARGET,
  {
    name: "lib-es2017",
    typeRoots: ["../node_modules/@types", "custom-typings"],
    typescript: {
      compilerOptions: {
        target: "es2017",
        lib: ["es2015", "dom"]
      },
      typescript: typescript,
      tsconfigJson: ["lib/tsconfig.json"]
    }
  }
);

// `lib-test` target
const libTestTarget = Object.assign(
  {},
  buildTools.config.LIB_TEST_TARGET,
  {
    name: "lib-test",
    scripts: ["test/**/*.ts", "lib/**/*.ts"],
    typeRoots: ["../node_modules/@types", "custom-typings"],
    typescript: {
      compilerOptions: {
        target: "es2017"
      },
      typescript: typescript,
      tsconfigJson: ["test/tsconfig.json"]
    },
  }
);

// `main` target
const mainTarget = Object.assign(
  {},
  buildTools.config.LIB_TARGET,
  {
    name: "main",
    scripts: ["main/**/*.ts", "lib/**/*.ts", "!**/*.spec.ts"],
    typeRoots: ["../node_modules/@types", "custom-typings"],
    typescript: {
      compilerOptions: {
        target: "es2017"
      },
      typescript: typescript,
      tsconfigJson: ["main/tsconfig.json"]
    }
  }
);

buildTools.projectTasks.registerAll(gulp, projectOptions);
buildTools.targetGenerators.node.generateTarget(gulp, projectOptions, mainTarget);
buildTools.targetGenerators.node.generateTarget(gulp, projectOptions, libTarget);
buildTools.targetGenerators.test.generateTarget(gulp, projectOptions, libTestTarget);

gulp.task("all:tsconfig.json", gulp.parallel("lib-es2017:tsconfig.json", "lib-test:tsconfig.json", "main:tsconfig.json"));
gulp.task("all:build", gulp.parallel("lib-es2017:build", "lib-test:build", "main:build"));
gulp.task("all:dist", gulp.parallel("lib-es2017:dist"));
