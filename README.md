# Semantic types

This package provide aliases for Typescript types to provide more semantic, mainly for strings and numbers.
It currently mainly defines aliases for numbers.

## Changelog

See [CHANGELOG.md](./CHANGELOG.md).

## License

[MIT License © 2017 Charles Samborski](./LICENSE.md)
